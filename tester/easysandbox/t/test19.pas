{memory allocation should work}
var
	p : ^integer;

begin
	writeln(0);
	new(p);
	writeln(1);
	p^ := 2;
	writeln(p^);
	dispose(p);
	writeln(3);
end.