unit passand;

interface
	procedure exitFunc(exitCode : integer);
	procedure halt(errnum : longint);
	procedure halt;
implementation

procedure wrapper_init; cdecl; external;
procedure wrapper_allocHeap; cdecl; external;
procedure exitfuncimpl(val : integer); cdecl; external name 'exit';
procedure exitFunc(exitCode : integer);
begin
	exitfuncimpl(exitCode);
end;

procedure halt;
begin
	flush(output);
	halt(0);
end;

procedure halt(errnum : longint);
begin
	exitfuncimpl(errnum);
end;
 
Const
	libc = 'libc';
 
Function Malloc (Size : ptrint) : Pointer; cdecl; external libc name 'malloc';
Procedure Free (P : pointer); cdecl; external libc name 'free';
function ReAlloc (P : Pointer; Size : ptrint) : pointer; cdecl; external libc name 'realloc';
Function CAlloc (unitSize,UnitCount : ptrint) : pointer; cdecl; external libc name 'calloc';
 
type
	pptrint = ^ptrint;
 
Function CGetMem	(Size : longword) : Pointer;
begin
	CGetMem:=Malloc(Size+sizeof(longword));
	if (CGetMem <> nil) then
	begin
		pptrint(CGetMem)^ := size;
		inc(CGetMem,sizeof(longword));
	end;
end;
 
Function CFreeMem (P : pointer) : longword;
begin
	if (p <> nil) then
		dec(p,sizeof(longword));
	Free(P);
	CFreeMem:=0;
end;
 
Function CFreeMemSize(p:pointer;Size:longword):longword;
begin
	if size<=0 then
	begin
		if size<0 then
			runerror(204);
		exit;
	end;
	if (p <> nil) then
	begin
		if (size <> plongword(p-sizeof(longword))^) then
			runerror(204);
	end;
	CFreeMemSize:=CFreeMem(P);
end;
 
Function CAllocMem(Size : longword) : Pointer;
begin
	CAllocMem:=calloc(Size+sizeof(longword),1);
	if (CAllocMem <> nil) then
	begin
		plongword(CAllocMem)^ := size;
		inc(CAllocMem,sizeof(longword));
	end;
end;
 
Function CReAllocMem (var p:pointer;Size:longword):Pointer;
begin
	if size=0 then
	begin
		if p<>nil then
		begin
			dec(p,sizeof(longword));
			free(p);
			p:=nil;
		end;
	end
	else
	begin
		inc(size,sizeof(longword));
		if p=nil then
			p:=malloc(Size)
		else
		begin
			dec(p,sizeof(longword));
			p:=realloc(p,size);
		end;
		if (p <> nil) then
		begin
			plongword(p)^ := size-sizeof(longword);
			inc(p,sizeof(longword));
		end;
	end;
	CReAllocMem:=p;
end;
 
Function CMemSize (p:pointer): longword;
 
begin
	CMemSize:=plongword(p-sizeof(longword))^;
end;
 
function CGetHeapStatus:THeapStatus;
 
var res: THeapStatus;
 
begin
	fillchar(res,sizeof(res),0);
	CGetHeapStatus:=res;
end;
 
function CGetFPCHeapStatus:TFPCHeapStatus;
 
begin
	fillchar(CGetFPCHeapStatus,sizeof(CGetFPCHeapStatus),0);
end;
 
Const
	CMemoryManager : TMemoryManager =
	(
		NeedLock : false;
		GetMem : @CGetmem;
	 	FreeMem : @CFreeMem;
		FreememSize : @CFreememSize;
		AllocMem : @CAllocMem;
		ReallocMem : @CReAllocMem;
		MemSize : @CMemSize;
		InitThread : Nil;
		DoneThread : Nil;
		RelocateHeap : Nil;
		GetHeapStatus : @CGetHeapStatus;
		GetFPCHeapStatus: @CGetFPCHeapStatus;
	);
var 
	inp:text;
	inputfilename:string;
Initialization
begin
	readln(inputfilename);
	assign(inp,inputfilename);
	reset(inp);
	input:=inp;
	wrapper_allocHeap();
	SetMemoryManager (CmemoryManager);
	wrapper_init;
end;
end.
